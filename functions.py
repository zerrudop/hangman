from turtle import *

class LastTurtle(object):
    def __init__(self) -> None:
        self.lastTurtle: Turtle = None
    
    def changeTurtle(self, newTurtle: Turtle):
        self.lastTurtle = newTurtle
        
    def clear(self):
        self.lastTurtle.clear()
        
def turnOutputListIntoString(outputList):
    return " ".join(outputList)
    
def make_petal():
    circle(20,270)
    right(180)

def make_center():
    circle(20)
    right(90)

def make_stem():
    for x in range(10):
        right(2)
        forward(10)
        
def drawBoard(turtle: LastTurtle, string: str):
    if turtle.lastTurtle != None:
        turtle.clear()
    newTurle = Turtle()
    newTurle.hideturtle()
    newTurle.penup()
    newTurle.goto(-50, -200)
    newTurle.pendown()
    newTurle.write(string, font=("Arial", 24, "normal"))
    turtle.lastTurtle = newTurle
    
def drawIncorrectGuesses(turtle: LastTurtle, string: str):
    if turtle.lastTurtle != None:
        turtle.clear()
    newTurle = Turtle()
    newTurle.hideturtle()
    newTurle.penup()
    newTurle.goto(0, 200)
    newTurle.pendown()
    newTurle.write(string, font=("Arial", 24, "normal"))
    turtle.lastTurtle = newTurle
        