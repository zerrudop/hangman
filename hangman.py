from turtle import *
import random
from functions import *



def main():
    words = ["judgment", "surprise", "union", "command", "discovery", "velvet", "twin", "dentist", "museum", "twitch"]
    
    word = random.choice(words)
    
    incorrectGuesses = 0
    
    turtleOutput = ["_"] * len(word)
    print(word)
    
    incorrectLetters = []
    
    lastTurtle: LastTurtle = LastTurtle()
    inncorrectTurtleGuesses: LastTurtle = LastTurtle()
    
    while incorrectGuesses < 6:
        letter = textinput("Sup","Choose a letter: ")
        
        if len(letter) != 1 or not letter.isalpha():
            continue

        if letter in word:
            print("Correct Guess")
            for i, char in enumerate(word):
                if char == letter:
                    turtleOutput[i] = letter   
        else:
            print("Incorrect Guess")
            incorrectLetters.append(letter)
            incorrectGuesses += 1 
            if incorrectGuesses == 1:
                make_center()
            elif incorrectGuesses == 6:
                make_stem()
            else:
                make_petal()
            
        if "".join(turtleOutput) == word:
            print(f"You Win! The word was {word}")   
            drawBoard(lastTurtle, turnOutputListIntoString(turtleOutput)) 
            break
        
        drawBoard(lastTurtle, turnOutputListIntoString(turtleOutput))
        drawIncorrectGuesses(inncorrectTurtleGuesses, ",".join(incorrectLetters))

    else:
        print(f"You Lose! The correct word was {word}")  
    
    
    
if __name__ == "__main__":
    hideturtle()
    main()
    done()